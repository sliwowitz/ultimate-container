image: gcc:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CATCH2_VERSION: v3.4.0
  CATCH2_INSTALL_PREFIX: ${CI_PROJECT_DIR}/build/catch2
  CODECOV_TOKEN: ${CODECOV_TOKEN}

stages:
  - prepare
  - build
  - test
  - coverage

before_script:
  - apt-get update
  - apt-get upgrade -y
  - apt-get install -y cmake lcov libhdf5-dev git

prepare:
  stage: prepare
  script:
    - git clone -b ${CATCH2_VERSION} https://github.com/catchorg/Catch2.git
    - cd Catch2
    - cmake -Bbuild -H. -DBUILD_TESTING=OFF -DCMAKE_INSTALL_PREFIX=${CATCH2_INSTALL_PREFIX}
    - cmake --build build/ --target install
  artifacts:
    paths:
      - ${CATCH2_INSTALL_PREFIX}
    expire_in: 1 hour

build-release:
  stage: build
  script:
    - mkdir -p build/release && cd build/release
    - >
      cmake
      -DCatch2_DIR=${CATCH2_INSTALL_PREFIX}/lib/cmake/Catch2
      -DCMAKE_BUILD_TYPE=Release
      -DUC_BUILD_UNIT_TESTS=ON
      -DUC_BUILD_EXAMPLES=ON
      -DUC_WITH_HDF5=ON
      ../..
    - make
  dependencies:
    - prepare
  artifacts:
    paths:
      - build/release/
      - ${CATCH2_INSTALL_PREFIX}
    expire_in: 1 hour

build-coverage:
  stage: build
  script:
    - mkdir -p build/coverage && cd build/coverage
    - cmake
      -DCatch2_DIR=${CATCH2_INSTALL_PREFIX}/lib/cmake/Catch2
      -DUC_BUILD_UNIT_TESTS=ON
      -DUC_BUILD_EXAMPLES=ON
      -DUC_WITH_HDF5=ON
      -DUC_ENABLE_COVERAGE=ON
      ../..
    - make
  artifacts:
    paths:
      - build/coverage
      - ${CATCH2_INSTALL_PREFIX}
    expire_in: 1 hour

unit:
  stage: test 
  script:
    - cd build/release
    - ctest --output-on-failure
  dependencies:
    - build-release

examples:
  stage: test
  script:
    - cd build/release
    - ./examples/nbody/nbody
    - ./examples/fdtd/fdtd num_steps=20 num_cells="20,20,20"
    - ./examples/hdf5/hdf5
  dependencies:
    - build-release

# Keep coverage in a separate stage so that it is not uploaded to Codecov 
# if any of the previous stages fails.
coverage:
  stage: coverage
  script:
    - cd build/coverage
    - ctest
    - lcov --capture --directory . --output-file coverage.info
    - lcov --remove coverage.info '/usr/*' '*tests/*' '*build/catch2/*' --output-file coverage.info
    - lcov --extract coverage.info '*include/*' --output-file coverage.info
    - lcov --list coverage.info
  artifacts:
    paths:
      - build/coverage/coverage.info
    expire_in: 1 week
  after_script:
    - >
      bash <(curl -s https://codecov.io/bash) 
      -f build/coverage/coverage.info 
      -t ${CODECOV_TOKEN}
  dependencies:
    - build-coverage

