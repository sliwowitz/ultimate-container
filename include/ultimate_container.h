//
// Created by strazce on 04/07/23.
//

#pragma once

#include "ultimate_container/vector/vector.h"
#include "ultimate_container/container_soa/container_soa.h"
#include "ultimate_container/container_soa/container_free_ops.h"

#ifdef USING_ULTIMATE_OPERATIONS
using namespace ultimate_container::vector::operations;
#endif

#ifdef HAS_HDF5
#include "ultimate_container/serialization/hdf5.h"
#endif
