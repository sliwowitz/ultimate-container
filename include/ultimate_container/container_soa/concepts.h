//
// Created by strazce on 26/07/23.
//

#pragma once

/** SoaReady is a concept that checks if the type is ready to be stored in SoA format.
 *
 * A type T is deemed SoA ready if it implements the T::soa_ready type alias.
 *
 * @tparam T Type to be checked.
 */
template <typename T>
concept SoaReady = requires { typename T::soa_ready; };

/** SoaTraits is a concept that checks if the type is usable inside a SoA container.
 *
 * A type T is deemed SoA container if it implements the following type aliases:
 * - T::value_type
 * - T::reference
 * - T::const_reference
 * - T::soa_tuple
 *
 * @tparam T Type to be checked.
 */
template <typename T>
concept SoaTraits = requires {
    typename T::value_type;
    typename T::reference;
    typename T::const_reference;
    typename T::soa_tuple;
};
