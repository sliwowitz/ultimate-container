#pragma once

#include "ultimate_container/vector/vector.h"

namespace ultimate_container::container_soa {
    namespace details {
        template<
                std::size_t ORDER,
                typename CONTAINER,
                std::size_t CDIM,
                typename IDX,
                typename DIFF,
                Arithmetic T>
        struct GradImpl {
        };

        template<typename CONTAINER, typename IDX, typename DIFF, Arithmetic T>
        struct GradImpl<1, CONTAINER, 3, IDX, DIFF, T>{
            inline vector::Vector<T, 3> operator()(const CONTAINER &f, const IDX &index, const DIFF &inverse_dr) {
                const int i = index[0];
                const int j = index[1];
                const int k = index[2];

                const int ii = index[0] + 1;
                const int jj = index[1] + 1;
                const int kk = index[2] + 1;

                return vector::Vector<T, 3>(
                        (f[IDX(ii, j, k)] - f[IDX(i, j, k)]) * inverse_dr[0],
                        (f[IDX(i, jj, k)] - f[IDX(i, j, k)]) * inverse_dr[1],
                        (f[IDX(i, j, kk)] - f[IDX(i, j, k)]) * inverse_dr[2]
                );
            }
        };

        template<typename CONTAINER, typename IDX, typename DIFF, Arithmetic T>
        struct GradImpl<1, CONTAINER, 2, IDX, DIFF, T>{
            inline vector::Vector<T, 2> operator()(const CONTAINER &f, const IDX &idx, const DIFF &idr) {
                const int i = idx[0];
                const int j = idx[1];

                const int ii = idx[0] + 1;
                const int jj = idx[1] + 1;

                return vector::Vector<T, 2>(
                        (f[IDX(ii, j)] - f[IDX(i, j)]) * idr[0],
                        (f[IDX(i, jj)] - f[IDX(i, j)]) * idr[1]
                );
            }
        };

        template<typename CONTAINER, typename IDX, typename DIFF, Arithmetic T>
        struct GradImpl<1, CONTAINER, 1, IDX, DIFF, T>{
            inline vector::Vector<T, 1> operator()(const CONTAINER &f, const IDX &idx, const DIFF &idr) {
                const int i = idx[0];

                const int ii = idx[0] + 1;

                return vector::Vector<T, 2>(
                        (f[IDX(ii)] - f[IDX(i)]) * idr[0]
                );
            }
        };

    }


    template<std::size_t Order, typename T, size_t DIM, typename IDX, typename DIFF>
    inline auto static grad(const ContainerSoa<T, DIM> &field, const IDX &index, const DIFF &inverse_dr) {
        details::GradImpl<Order, ContainerSoa<T, DIM>, DIM, IDX, DIFF, T> grad_impl;
        return grad_impl(field, index, inverse_dr);
    }

}
