#pragma once

#include "ultimate_container/vector/vector.h"

namespace ultimate_container::container_soa {
    namespace details {
        template<typename CONTAINER,
                size_t DIM,
                typename IDX,
                typename DIFF,
                Arithmetic T>
        struct LaplaceImpl;

        template<typename CONTAINER, typename IDX, typename DIFF, Arithmetic T>
        struct LaplaceImpl<CONTAINER, 3, IDX, DIFF, T>{
            inline T operator()(const CONTAINER &f, const IDX &idx, const DIFF &idr) {
                const int i = idx[0];
                const int j = idx[1];
                const int k = idx[2];

                const int ii = idx[0] + 1, im = idx[0] - 1;
                const int jj = idx[1] + 1, jm = idx[1] - 1;
                const int kk = idx[2] + 1, km = idx[2] - 1;

                T l = (
                        (f[IDX(ii, j, k)] - 2*f[idx] + f[IDX(im, j, k)]) * sqr(idr[0]) +
                        (f[IDX(i, jj, k)] - 2*f[idx] + f[IDX(i, jm, k)]) * sqr(idr[1]) +
                        (f[IDX(i, j, kk)] - 2*f[idx] + f[IDX(i, j, km)]) * sqr(idr[2])
                );

                return l;
            }
        };
    }

    template<typename T, size_t DIM, typename IDX, typename DIFF>
    inline T static laplace(const ContainerSoa<T, DIM> &f, const IDX &idx, const DIFF &idr) {
        details::LaplaceImpl<ContainerSoa<T, DIM>, DIM, IDX, DIFF, T> laplace_impl;
        return laplace_impl(f, idx, idr);
    }

}