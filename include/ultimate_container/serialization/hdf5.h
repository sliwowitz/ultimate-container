#pragma once

#include "hdf5_helpers.h"
#include "hdf5_c_api.h"

#include "ultimate_container/container_soa/container_soa.h"
#include "ultimate_container/utils/concepts.h"
#include "ultimate_container/utils/string.h"
#include "ultimate_container/vector/vector.h"

#include <H5Cpp.h>

#include <concepts>
#include <utility>
#include <vector>
#include <string>
#include <string_view>


namespace ultimate_container::serialization {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Winline"

void debug([[maybe_unused]] const std::string& msg) {
#ifndef NDEBUG
    std::cout << "HDF5: " << msg << std::endl;
#endif
}

using FileType = H5::H5File;

auto create_output_file(const std::string& basename) {
    FileType file(basename + ".h5", H5F_ACC_TRUNC);
    return file;
}

template<Arithmetic T, std::integral IndexD, std::integral IndexM, std::size_t DIM>
void serialize_flat_array(const std::vector<T>& data,
                          const std::array<IndexD, DIM>& dims,
                          const std::array<IndexM, DIM>& min,
                          const std::array<IndexM, DIM>& length,
                          const H5::Group& group,
                          const std::string& name) {
    // Reverse the order of dimensions
    auto reversed_dims = dims;
    std::reverse(reversed_dims.begin(), reversed_dims.end());

    // Create a memory dataspace
    H5::DataSpace mem_dataspace(DIM, convert_to_hsize_t(reversed_dims).data());

    // Define the hyperslab parameters
    std::array<hsize_t, DIM> count = convert_to_hsize_t(length);
    std::array<hsize_t, DIM> start = convert_to_hsize_t(min);
    std::array<hsize_t, DIM> stride;
    stride.fill(1);
    std::array<hsize_t, DIM> block;
    block.fill(1);

    // Reverse the order of hyperslab parameters
    std::reverse(count.begin(), count.end());
    std::reverse(start.begin(), start.end());
    std::reverse(stride.begin(), stride.end());
    std::reverse(block.begin(), block.end());

    // Select the hyperslab and write the data
    mem_dataspace.selectHyperslab(H5S_SELECT_SET, count.data(), start.data(), stride.data(), block.data());
    H5::DataSpace file_dataspace(DIM, count.data());
    H5::DataType datatype = NativeTypeFor<T>::type();
    H5::DataSet dataset = group.createDataSet(name.c_str(), datatype, file_dataspace);
    dataset.write(&data[0], datatype, mem_dataspace, file_dataspace);
}

H5::Group create_group_from_path(FileType& file, const std::vector<std::string>& path_tokens) {
    /* H5::Group acts as a handle, and shouldn't be copied or reassigned. Therefore, we always attempt to
     * create the group under the full File context, and then return the last group created or opened.
     * While the C library has a H5P_SET_CREATE_INTERMEDIATE_GROUP property, the C++ library seems not to
     * support it. Therefore, we have to create the groups one by one.
     */
    for (size_t i = 0; i < path_tokens.size(); ++i) {
        std::string full_path = utils::join(std::vector<std::string>(path_tokens.begin(), path_tokens.begin() + i + 1), "/");
        if(!does_group_exist(file, full_path)) {
            debug("Creating group \"" + full_path + "\"");
            file.createGroup(full_path);
        }
    }
    return file.openGroup(utils::join(path_tokens, "/"));   // Returns the last group created or opened
}

template<typename T, std::size_t N>
void create_and_write_attribute(H5::Group& parent_group,
                                const std::string& name,
                                const H5::DataSpace& dataspace,
                                const std::array<T, N>& value)
{
    H5::Attribute attribute = parent_group.createAttribute(name, H5::PredType::NATIVE_HSIZE, dataspace);
    attribute.write(H5::PredType::NATIVE_HSIZE, convert_to_hsize_t(value).data());
}

template<std::size_t DIM, typename Index>
void serialize_arithmetic_component(const auto& component,
                                    FileType& file,
                                    const std::vector<std::string>& path_tokens,
                                    const vector::Vector<Index, DIM>& min,
                                    const vector::Vector<Index, DIM>& max) {
    using namespace vector::operations;
    // Set up the HDF5 group and dataset names.
    std::string dataset_name = path_tokens.back();
    H5::Group group = (path_tokens.size() > 1)
                      ? create_group_from_path(file, {path_tokens.begin(), path_tokens.end() - 1})
                      : file.openGroup("/");

    // Extract the flattened std::vector<T> and serialize
    component.apply_to_components_with_nesting(
            [&](const auto &flat_component, [[maybe_unused]] std::string_view flat_variable_path) {
                serialize_flat_array(flat_component,
                                     component.size(),
                                     min.to_array(),
                                     (max - min).to_array(),
                                     group,
                                     dataset_name);
            }, utils::join(path_tokens, "/")
    );
}

template<typename T, std::size_t DIM, typename Index = std::size_t>
void serialize_to_hdf5_impl(const container_soa::ContainerSoa<T, DIM>& container,
                            FileType& file,
                            std::string_view parent_group_name = "",
                            const vector::Vector<Index, DIM>& min = vector::Vector<Index, DIM>::Zeros(),
                            const vector::Vector<Index, DIM>& max = vector::Vector<Index, DIM>::MaxVal()) {

    if constexpr (Arithmetic<typename std::decay_t<decltype(container)>::value_type>){
        /* TODO: Issue #10: This branch is executed only when the top ContainerSoa holds Arithmetic<T>.
         *       The algorithm should be modified so that this is executed in the same if branch as the case where one
         *       of the container's components is artihmetic. We need to shift the decision to a different point
         *       in the source.
         *
         * TODO: Issue #11 Writing attributes: Now they are appended to the top-level group, but since
         *       a 1D component would normally be written as a dataset, the algorithm would fail here as it
         *       has already created a group with name 'parent_group_name' and would now attempt to create a dataset
         *       with the same path. We manually append "/0" to the path as a workaround. */
        serialize_arithmetic_component(container, file, utils::split(std::string(parent_group_name) + "/0", "/"), min, max);
    } else {
        container.apply_to_components_with_nesting([&](const auto &component, std::string_view variable_path) {
            auto tokens = utils::split(std::string(variable_path), "/");
            using ComponentType = std::decay_t<decltype(component)>;
            if constexpr (Arithmetic<typename ComponentType::value_type>) {
                serialize_arithmetic_component(component, file, tokens, min, max);
            } else {
                serialize_to_hdf5_impl(component, file, variable_path, min, max);
            }
        }, parent_group_name);
    }
}

    template<typename T, std::size_t DIM, typename Index = std::size_t>
    void serialize_to_hdf5(const container_soa::ContainerSoa<T, DIM>& container,
                           FileType& file,
                           std::string_view parent_group_name = "",
                           const vector::Vector<Index, DIM>& min_in = vector::Vector<Index, DIM>::Zeros(),
                           const vector::Vector<Index, DIM>& max_in = vector::Vector<Index, DIM>::MaxVal()) {

        using namespace vector;
        using namespace vector::operations;

        // Prepare the min and max bounds
        auto min = cwise_max(min_in, Vector<Index, DIM>::Zeros());
        auto max = cwise_min(max_in, vector_cast<Index>(make_vector(container.size())));

        // Write the attributes to the master parent_group
        H5::Group parent_group = create_group_from_path(file, utils::split(std::string(parent_group_name), "/"));

        // Create a DataSpace with a single dimension of size DIM for the attributes
        hsize_t dims[1] = {DIM};
        H5::DataSpace attr_dataspace = H5::DataSpace(1, dims);

        create_and_write_attribute(parent_group, "container_size", attr_dataspace, (max - min).to_array());
        create_and_write_attribute(parent_group, "original_container_size", attr_dataspace, container.size());
        create_and_write_attribute(parent_group, "original_min_bound", attr_dataspace, min.to_array());
        create_and_write_attribute(parent_group, "original_max_bound", attr_dataspace, max.to_array());

        // Start serializing the container
        serialize_to_hdf5_impl(container, file, parent_group_name, min, max);
    }

#pragma GCC diagnostic pop

}

