#pragma once

#include <H5Cpp.h>
#include <H5Lpublic.h>
#include <H5Spublic.h>
#include <concepts>

namespace ultimate_container::serialization {

    template<std::integral Index, std::size_t DIM>
    std::array<hsize_t, DIM> convert_to_hsize_t(const std::array<Index, DIM>& arr) {
        std::array<hsize_t, DIM> converted;
        std::ranges::transform(arr, converted.begin(), [](Index val) {
            return static_cast<hsize_t>(val);
        });
        return converted;
    }

    bool does_group_exist(H5::H5File file, const std::string &group_path) {
        return H5Lexists(file.getId(), group_path.c_str(), H5P_DEFAULT);
    }

    template<std::integral IndexD, std::integral IndexM, std::size_t N>
    hsize_t create_dataspace(const std::array<IndexD, N>& dims,
                             const std::array<IndexM, N>& min,
                             const std::array<IndexM, N>& max) {
        auto converted_dims = convert_to_hsize_t(dims);
        auto converted_min = convert_to_hsize_t(min);
        auto converted_max = convert_to_hsize_t(max);
        // Create a simple dataspace with the dimension size, min and max
        hsize_t dataspace_id = H5Screate_simple(N, converted_dims.data(), nullptr);
        H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, converted_min.data(), nullptr, converted_max.data(), nullptr);
        return dataspace_id;
    }

    void close_dataspace(hsize_t dataspace_id) {
        H5Sclose(dataspace_id);
    }

}
