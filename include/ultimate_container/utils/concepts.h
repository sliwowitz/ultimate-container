//
// Created by strazce on 26/07/23.
//

#pragma once

#include <type_traits>
#include <complex>

namespace ultimate_container {

    template<typename T, typename U>
    concept SameAs = std::is_same_v<std::remove_cv_t<std::remove_reference_t<T>>, U>;

    /** Arithmetic is a concept that checks if the type is an arithmetic type.
    *
    * @tparam T Type to be checked.
    */
    template<typename T>
    concept Arithmetic = std::is_arithmetic_v<T> ||
            SameAs<T, std::complex<float>> ||
            SameAs<T, std::complex<double>> ||
            SameAs<T, std::complex<long double>>;

    /** NonArithmetic is a concept that checks if the type is not an arithmetic type.
    *
    * @tparam T Type to be checked.
    */
    template<typename T>
    concept NonArithmetic = !Arithmetic<T>;

    template<typename T>
    concept NonReference = !std::is_reference_v<T>;

    template<typename T>
    concept NonConstReference = std::is_reference_v<T> && !std::is_const_v<std::remove_reference_t<T>>;

    template<typename T>
    concept IsWritable = (!std::is_reference_v<T> && !std::is_const_v<T>) || NonConstReference<T>;

    template<typename IDX>
    concept Indexable = requires(IDX idx) { { idx[0] } -> std::convertible_to<std::size_t>; };

    template<typename Vec>
    concept IsVector = requires(Vec v, const Vec cv, size_t i) {
        { v[i] } -> std::convertible_to<typename Vec::value_type>;
        { cv[i] } -> std::convertible_to<typename Vec::const_reference_element>;
        { v.size() } -> std::same_as<std::size_t>;
        { cv.size() } -> std::same_as<std::size_t>;
        { v.to_string() } -> std::same_as<std::string>;
    };
}

