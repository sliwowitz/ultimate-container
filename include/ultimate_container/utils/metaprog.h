//
// Created by strazce on 04/07/23.
//

#pragma once

#include "concepts.h"
#include <memory>


namespace ultimate_container::utils {
    template<int N, typename V>
    struct gen_tuple {
        using type =

        decltype (
        std::tuple_cat(std::declval<std::tuple<V>>(),
                       std::declval<typename
                       gen_tuple<N - 1, V>::type
                       >()));
    };

    template<typename V>
    struct gen_tuple<0, V> {
        using type = std::tuple<>;
    };

    template <size_t... Indices>
    void unroll (auto f, std::index_sequence<Indices...>)
    {
        if constexpr (sizeof...(Indices) > 1) {
            (f.template operator()<Indices>(), ...);
        } else if constexpr (sizeof...(Indices) == 1) { // Single index would have nothing to expand on the right side of the comma operator, emitting a warning.
            f.template operator()<0>();
        } else {
            // No indices -> nothing to unroll. This is a safeguard against empty sequences.
        }
    }
}

