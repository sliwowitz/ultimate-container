#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <ranges>
#include <iterator>

namespace ultimate_container::utils {

    std::vector<std::string> split(const std::string& str, const std::string& delimiter) {
        std::vector<std::string> result;
        for (auto&& sub_range : std::views::split(str, delimiter)) {
            result.emplace_back(sub_range.begin(), sub_range.end());
        }
        return result;
    }

    template <std::ranges::input_range Range>
    std::string join(const Range& range, const std::string& delimiter) {
        if (range.empty()) return "";

        auto join_with_delimiter = std::views::join_with(delimiter);
        auto joined_view = range | join_with_delimiter;

        return {std::ranges::begin(joined_view), std::ranges::end(joined_view)};
    }

}