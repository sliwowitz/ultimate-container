#pragma once

#include <array>
#include <algorithm>
#include <cassert>
#include <limits>
#include <ostream>
#include <vector>

#include "vector_expressions.h"
#include "../utils/concepts.h"

namespace ultimate_container::vector {

    //TODO: Alignment according to the CPU architecture.
    //#define ALIGN_ULTIMATE_VECTOR

    #ifdef ALIGN_ULTIMATE_VECTOR
    constexpr auto ALIGN = 32;
    #endif


    template<typename Inner, std::size_t DIM>
    using Expr = expressions::Expressions<Inner, DIM>;

    template<typename T, std::size_t DIM>
    struct
    #ifdef ALIGN_ULTIMATE_VECTOR
    alignas(ALIGN)
    #endif
    Vector : Expr<T, DIM> {
        static constexpr std::size_t dimension = DIM;
        using soa_ready = std::true_type;

        using value_type = typename Details<T>::value_type; ///< The type of the elements stored in the vector
        using reference_element = typename Details<T>::reference; ///< The reference to elements stored in the vector
        using const_reference_element = typename Details<T>::const_reference; ///< The const reference to elements stored in the vector

        using value_vector = Vector<value_type, DIM>; ///< The type of the vector with the same value type
        using const_reference_vector = Vector<const_reference_element, DIM>; ///< The type of the vector with const references
        using reference_vector = Vector<reference_element, DIM>; ///< The type of the vector with references

        // Store in reference_wrapper if T is a reference, otherwise use pure T.
        using inner_storage = typename std::conditional_t<
                std::is_reference_v<T>,
                std::reference_wrapper<
                    std::conditional_t<
                            std::is_const_v<std::remove_reference_t<T>>,
                            const value_type,
                            value_type
                            >
                        >,
                    T>;
        using Storage = std::array<inner_storage, DIM>;

        auto&& operator[](this auto&& self, std::size_t k) {
            return std::forward<decltype(self)>(self).elem(k);
        }

        template <std::size_t I>
        auto&& get(this auto&& self) {
            static_assert(I < DIM, "Index out of range");
            return std::forward<decltype(self)>(self).elem(I);
        }

        auto&& data(this auto&& self) {
            return std::forward<decltype(self)>(self)._data;
        }

        //TODO: also provide begin/end iterators
        //TODO: hides size() from Expression<> base class
        constexpr auto size() const { return DIM; }

        //=== CONSTRUCTORS
        constexpr Vector(){
            static_assert(!std::is_reference_v<T>, "Default constructor is deleted for reference vectors");
        }

        /// Specialized constructor for single element vectors
        template <typename U = T>
        requires (!std::is_reference_v<U> && DIM == 1)
        constexpr Vector(const T &value)
                : _data{{value}} {}

        /// Constructing a N-Vector of values by a N-parameter constructor
        template <typename... Args>
        requires ( !std::is_reference_v<T>
                && sizeof...(Args) == DIM-1
                && DIM > 1
                && (std::is_convertible_v<Args, T> && ...)
                )
        constexpr explicit Vector(const T &head, Args const &... in)  // Explicitly specifying "T head" prevents ambiguous overloads with constructors taking Vectors
                : _data{{head, in...}} {}

        /// Constructing a N-Vector of references by a N-parameter constructor
        template <typename... Args>
        requires ( std::is_reference_v<T>
                && sizeof...(Args) == DIM
                && (... && !std::is_same_v<std::remove_cvref_t<Args>, Vector<T, DIM>>)
                )
        constexpr explicit Vector(Args&&... args)
                : _data{{std::forward<Args>(args)...}} {}

        /// Copy constructor
        constexpr Vector(const Vector &v)
        requires std::is_reference_v<T>
                : _data(v.data()) { }

        /// Move constructor
        constexpr Vector(Vector &&v) noexcept
        requires std::is_reference_v<T>
                : _data(std::move(v.data())) { }

        /// Constructor from another Vector of potentially different type
        template<typename T2>
        requires (!std::is_reference_v<T> && std::is_convertible_v<T2, T>)
        constexpr Vector(const Vector<T2, DIM>& v) {
            for (std::size_t i = 0; i < DIM; ++i) {
                _data[i] = static_cast<T>(v[i]);
            }
        }

        /// Constructor from another Vector of potentially different reference type
        template<typename T2>
        requires (!std::is_reference_v<T> && std::is_convertible_v<T2, T>)
        constexpr explicit Vector(const Vector<T2&, DIM>& v) {
            for (std::size_t i = 0; i < DIM; ++i) {
                _data[i] = static_cast<T>(v[i]);
            }
        }

        /// Construct from std::array
        template<typename T2>
        requires (!std::is_reference_v<T> && std::is_convertible_v<T2, T>)
        constexpr explicit Vector(const std::array<T2, DIM> &arr) {
            for(std::size_t i=0; i<DIM; ++i) elem(i) = arr[i];
        }

        /// Construct from std::initializer_list
        template<typename U = T>
        requires (!std::is_reference_v<U>)
        constexpr Vector(std::initializer_list<T> init_list) {
            assert(init_list.size() == DIM);
            std::copy(init_list.begin(), init_list.end(), _data.begin());
        }

        /// Copy assignment operator
        Vector& operator=(const Vector& other)
        requires (!std::is_reference_v<T>) {
            if (this != &other) {
                std::copy(other._data.begin(), other._data.end(), _data.begin());
            }
            return *this;
        }

        /// Move assignment operator
        Vector& operator=(Vector&& other) noexcept
        requires (!std::is_reference_v<T>) {
            if (this != &other) {
                _data = std::move(other._data);
            }
            return *this;
        }

        //=== ASSIGNMENT OPERATORS
        template<typename T2>
        Vector& operator=(const Vector<T2, DIM> &v) {
            for(std::size_t i=0; i<DIM; ++i) elem(i) = v[i];
            return *this;
        }

        template<typename T2>
        Vector& operator=(Vector<T2, DIM> &&v) {
            for(std::size_t i=0; i<DIM; ++i) elem(i) = std::move(v[i]);
            return *this;
        }

        template<typename T2>
        requires (!std::is_reference_v<T>)   // This disallows creating a reference to a reference
        Vector& operator=(const Vector<T2&, DIM> &v) {
            for(std::size_t i=0; i<DIM; ++i) elem(i) = v[i];
            return *this;
        }

        template<typename T2> // This disallows creating a reference to a reference, using the enable_if/int trick to deduce T2
        requires (!std::is_reference_v<T>)   // This disallows creating a reference to a reference
        Vector& operator=(const std::array<T2, DIM> &arr) {
            std::copy(arr.begin(), arr.end(), _data.begin());
            return *this;
        }

        operator std::vector<inner_storage>() const
        {
            return std::vector<inner_storage>(this->_data.begin(), this->_data.end());
        }

        //=== IMPORTANT CONSTANTS
        template<size_t... Is>
        inline static constexpr auto constant_from_sequence(T c, std::index_sequence<Is...>)
        requires (!std::is_reference_v<T>) {
            return Vector<T,DIM>(c + static_cast<T>(Is-Is)...); // I'm being 'clever' here because I don't know how else to produce a sequence of zeros.
        }

        static constexpr inline Vector<T,DIM> Zeros() requires (!std::is_reference_v<T>) {
            return constant_from_sequence(static_cast<T>(0), std::make_index_sequence<DIM>{});
        }
        static constexpr inline Vector<T,DIM> Ones() requires (!std::is_reference_v<T>) {
            return constant_from_sequence(static_cast<T>(1), std::make_index_sequence<DIM>{});
        }
        static constexpr inline Vector<T,DIM> MaxVal() requires (!std::is_reference_v<T>) {
            return constant_from_sequence(std::numeric_limits<T>::max(), std::make_index_sequence<DIM>{});
        }
        static constexpr inline Vector<T,DIM> MinVal() requires (!std::is_reference_v<T>) {
            return constant_from_sequence(std::numeric_limits<T>::min(), std::make_index_sequence<DIM>{});
        }

        //=== ALGEBRA
        template<typename T2> constexpr bool operator==(const Vector<T2, DIM>& b) const {
            for(size_t i = 0; i<DIM; ++i) {
                if (elem(i) != b[i]){
                    return false;
                }
            }
            return true;
        }

        value_type squared_norm(){   //TODO: Performance test
            value_type ret = 0;
            for(std::size_t k = 0; k < size(); ++k) ret += elem(k)*elem(k);
            return ret;
        }

        value_type sum(){   //TODO: Performance test
            value_type ret = 0;
            for(std::size_t k = 0; k < size(); ++k) ret += elem(k);
            return ret;
        }

        value_type product(){   //TODO: Performance test
            value_type ret = 1;
            for(std::size_t k = 0; k < size(); ++k) ret *= elem(k);
            return ret;
        }

        template<typename V>
        inline auto cross(const V &b) const {
            static_assert(DIM == 3, "Cross product undefined for vectors of dimension != 3.");
            return Vector(
                    elem(1)*b[2] - elem(2)*b[1],
                    elem(2)*b[0] - elem(0)*b[2],
                    elem(0)*b[1] - elem(1)*b[0]
            );
        }

        template<typename V>
        inline auto dot(const V &b) const {
            value_type ret = 0;
            for(std::size_t k = 0; k < size(); ++k) ret += elem(k)*b[k];
            return ret;
        }


        //=== OTHER FUNCTIONS
        /** @brief Cast a vector to different scalar types.
         *
         * @tparam T2 Type of scalars in the returned vectors
         * @return New vector of the same size, but with members of type T2
         *
         * @todo Implement also as a unary Expression operator?
         */
        template<typename T2>
        inline auto cast() const
        requires (!std::is_reference_v<T2> && std::is_convertible_v<T, T2>) {
            return cast_by_sequence<T2>(std::make_index_sequence<DIM>{});
        }

        bool is_zero(){
            for(std::size_t k = 0; k < size(); ++k)
                if( elem(k) != 0)
                    return false;
            return true;
        }

        // Function to convert Vector to std::array
        std::array<T, DIM> to_array() const {
            return _data;
        }

        [[nodiscard]] std::string to_string() const {
            std::string output = "(";
            output += std::to_string(elem(0));
            for(std::size_t k = 1; k < size(); ++k) {
                output += ", " + std::to_string(elem(k));
            }
            output += ")";
            return output;
        }

        friend std::ostream &
        operator<<(std::ostream & co,  Vector<T, DIM> const & v) {
            co << v.to_string();
            return co;
        }

    private:
        Storage _data;

        auto&& elem(this auto&& self, size_t k) {
            if constexpr (std::is_reference_v<T>)
                return std::forward<decltype(self)>(self)._data[k].get();
            else
                return std::forward<decltype(self)>(self)._data[k];
        }

        /** Takes an integer sequence Is, and returns new vector made values casted to T2.
          */
        template<typename T2, size_t... Is>
        inline auto cast_by_sequence(std::index_sequence<Is...>) const {
            return Vector<T2, DIM>(static_cast<T2>(data()[Is])...);
        }
    };

}

namespace ultimate_container {
    // Vector factory
    template<typename T, std::size_t DIM>
    auto make_vector(const std::array<T, DIM>& arr) {
        vector::Vector<T, DIM> vec(arr);
        return vec;
    }
}

namespace std {
    // Specialization of std::tuple_size for Vector
    template<typename T, std::size_t N>
    struct tuple_size<::ultimate_container::vector::Vector<T, N>> : std::integral_constant<std::size_t, N> {};

    // Specialization of std::tuple_element for Vector
    template<std::size_t I, typename T, std::size_t N>
    struct tuple_element<I, ::ultimate_container::vector::Vector<T, N>> {
        using type = ::ultimate_container::vector::Vector<T, N>::value_type;
    };

    // Specialization of std::get for Vector
    template<std::size_t I, typename T, std::size_t N>
    constexpr auto& get(::ultimate_container::vector::Vector<T, N>& v) {
        return v.template get<I>();
    }

    template<std::size_t I, typename T, std::size_t N>
    constexpr const auto& get(const ::ultimate_container::vector::Vector<T, N>& v) {
        return v.template get<I>();
    }

    template<std::size_t I, typename T, std::size_t N>
    constexpr auto&& get(::ultimate_container::vector::Vector<T, N>&& v) {
        return std::move(v).template get<I>();
    }

    template<std::size_t I, typename T, std::size_t N>
    constexpr const auto&& get(const ::ultimate_container::vector::Vector<T, N>&& v) {
        return std::move(v).template get<I>();
    }
}

namespace std {
    template<typename T, std::size_t N>
    struct formatter<::ultimate_container::vector::Vector<T, N>> {
        constexpr auto parse(format_parse_context &ctx) {
            return ctx.begin();
        }

        auto format(const ::ultimate_container::vector::Vector<T, N> &v, format_context &ctx) const {
            return format_to(ctx.out(), "{}", v.to_string());
        }
    };
}

#include "vector_free_ops.h"
