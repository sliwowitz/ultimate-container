/*
 * vector_details.h
 *
 *  Created on: Apr 7, 2016
 *      Author: strazce
 */

#pragma once

#include <type_traits>

namespace ultimate_container::vector {

template<typename T>
struct Details {
    using value_type       = typename std::remove_const_t<typename std::remove_reference_t<T>>;
    using reference        = typename std::add_lvalue_reference_t<value_type>;
    using const_reference  = typename std::add_lvalue_reference_t<typename std::add_const_t<value_type>>;
};

}
