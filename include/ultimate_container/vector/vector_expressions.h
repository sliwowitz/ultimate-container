#pragma once

#include <cstdlib>
#include <cmath>
#include <type_traits>

#include "vector_details.h"

namespace ultimate_container::expressions {

template<typename T>
using Details = vector::Details<T>;

template<typename Inner, std::size_t DIM>
struct Expressions {

    using value_type      = typename Details<Inner>::value_type;
    using reference       = typename Details<Inner>::reference;
    using const_reference = typename Details<Inner>::const_reference;

    constexpr auto size() {
        return DIM;
    }

    template<typename V> constexpr
    auto operator+=(this auto&& self, const V& b) -> decltype(auto) {
        static_assert(DIM == std::remove_reference_t<V>::dimension, "Dimension mismatch!");
        for(std::size_t i=0; i<DIM; ++i){
            self[i] += b[i];
        }
        return self;
    }

    template<typename V> constexpr
    auto operator-=(this auto&& self, V&& b) -> decltype(auto) {
        static_assert(DIM == std::remove_reference_t<V>::dimension, "Dimension mismatch!");
        for(std::size_t i=0; i<DIM; ++i){
            self[i] -= b[i];
        }
        return self;
    }

    template<
            typename R,
            typename std::enable_if_t<std::is_arithmetic_v<R>, R>* = nullptr>
    constexpr
    auto operator*=(this auto&& self, R c) -> decltype(auto) {
        for(std::size_t i=0; i<DIM; ++i){
            self[i] *= c;
        }
        return self;
    }

    template<
            typename R,
            typename std::enable_if_t<std::is_arithmetic_v<R>, R>* = nullptr>
    constexpr
    auto operator/=(this auto&& self, R c) -> decltype(auto) {
        for(std::size_t i=0; i<DIM; ++i){
            self[i] /= c;
        }
        return self;
    }

    auto inverse(this auto&& self) -> decltype(auto)
    requires std::is_floating_point_v<value_type> {
        for(std::size_t i=0; i<DIM; ++i){
            self[i] = value_type{1}/self[i];
        }
        return self;
    }

    template<typename V> constexpr
    auto cwise_product(this auto&& self, V&& b) -> decltype(auto) {
        static_assert(DIM == std::remove_reference_t<V>::dimension, "Dimension mismatch!");
        for(std::size_t i=0; i<DIM; ++i){
            self[i] *= b[i];
        }
        return self;
    }

    template<typename V> constexpr
    auto cwise_divide(this auto&& self, V&& b) -> decltype(auto) {
        static_assert(DIM == std::remove_reference_t<V>::dimension, "Dimension mismatch!");
        for(std::size_t i=0; i<DIM; ++i){
            self[i] /= b[i];
        }
        return self;
    }

    template<typename V> constexpr
    auto cwise_min(this auto&& self, V&& b) -> decltype(auto) {
        static_assert(DIM == std::remove_reference_t<V>::dimension, "Dimension mismatch!");
        for(std::size_t i=0; i<DIM; ++i){
            self[i] = std::min(self[i],b[i]);
        }
        return self;
    }

    template<typename V> constexpr
    auto cwise_max(this auto&& self, V&& b) -> decltype(auto) {
        static_assert(DIM == std::remove_reference_t<V>::dimension, "Dimension mismatch!");
        for(std::size_t i=0; i<DIM; ++i){
            self[i] = std::max(self[i],b[i]);
        }
        return self;
    }

    constexpr auto cwise_sqr(this auto&& self) -> decltype(auto) {
        for(std::size_t i=0; i<DIM; ++i){
            self[i] *= self[i];
        }
        return self;
    }

    constexpr auto floor(this auto&& self) -> decltype(auto) {
        for(std::size_t i=0; i<DIM; ++i){
            self[i] = std::floor(self[i]);
        }
        return self;
    }

    constexpr auto cwise_abs(this auto&& self) -> decltype(auto) {
        for(std::size_t i=0; i<DIM; ++i){
            self[i] = std::abs(self[i]);
        }
        return self;
    }

};

}
