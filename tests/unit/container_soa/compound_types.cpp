#include "test_particle.h"

#include "container_soa/container_soa.h"
#include "vector/vector.h"

#include <catch2/catch_all.hpp>

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;

TEMPLATE_TEST_CASE("ContainerSoA for Compound Types", "[ContainerSoa][Compound]", float, double) {
    using TestVector = Vector<TestType, 3>;
    using TestParticle = Particle<TestVector, TestType>;

    constexpr std::size_t CONTAINER_DIM = 2;
    constexpr std::array<std::size_t, CONTAINER_DIM> dims = {8, 8};

    using TestContainer = ContainerSoa<TestParticle, CONTAINER_DIM>;

    TestContainer container(dims[0], dims[1]);
    REQUIRE(container.size() == dims);

    SECTION("Default Initialization") {
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t l = 0; l < 3; ++l) {
                    REQUIRE(container[i, j].position[l] == TestType{});
                }
                REQUIRE(container[i, j].mass == TestType{});
                REQUIRE(container[i, j].id == 0);
            }
        }
    }

    for (std::size_t i = 0; i < dims[0]; ++i) {
        for (std::size_t j = 0; j < dims[1]; ++j) {
            auto particle = TestParticle(TestVector{1, 2, 3},
                                         4,
                                         i * dims[1] + j);
            container[i, j] = particle;
        }
    }

    for (std::size_t i = 0; i < dims[0]; ++i) {
        for (std::size_t j = 0; j < dims[1]; ++j) {
            REQUIRE(container[i, j].position == TestVector{1, 2, 3});
            REQUIRE(container[i, j].mass == 4);
            REQUIRE(container[i, j].id == static_cast<int>(i * dims[1] + j));
        }
    }

    SECTION("Copy and modifications") {
        TestContainer copy = container;
        REQUIRE(copy.size() == container.size());

        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                copy[i, j].position += TestVector{1, 1, 1};
                copy[i, j].mass *= 2;
                copy[i, j].id += 1;
            }
        }

        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                // Copy should be modified
                REQUIRE(copy[i, j].position == TestVector{2, 3, 4});
                REQUIRE(copy[i, j].mass == 8);
                REQUIRE(copy[i, j].id == static_cast<int>(i * dims[1] + j + 1));
                // Original container should not be modified
                REQUIRE(container[i, j].position == TestVector{1, 2, 3});
                REQUIRE(container[i, j].mass == 4);
                REQUIRE(container[i, j].id == static_cast<int>(i * dims[1] + j));
            }
        }
    }

    SECTION("Move Initialization") {
        TestContainer moved = std::move(container);
        REQUIRE(moved.size() == dims);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                REQUIRE(moved[i, j].position == TestVector{1, 2, 3});
                REQUIRE(moved[i, j].mass == 4);
                REQUIRE(moved[i, j].id == static_cast<int>(i * dims[1] + j));
            }
        }
    }

}