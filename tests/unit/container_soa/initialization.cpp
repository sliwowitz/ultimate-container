#include "container_soa/container_soa.h"

#include <complex>

#include <catch2/catch_all.hpp>

using namespace ultimate_container::container_soa;

//TODO: Add all types including std::complex
TEMPLATE_TEST_CASE("ContainerSoA Initialization Tests", "[ContainerSoa][Initialization]", int, float, double, std::complex<float>, std::complex<double>) {
    using Scalar = TestType;

    constexpr std::size_t DIM = 3;
    constexpr std::array<std::size_t, DIM> dims = {10, 5, 2};

//    SECTION("Default Initialization") {
//        ContainerSoa<Scalar, DIM> container;
//        REQUIRE(container.size() == std::array<std::size_t, DIM>{0, 0, 0});
//    }

    SECTION("Initialization with Dimension Sizes") {
        ContainerSoa<Scalar, DIM> container(dims[0], dims[1], dims[2]);
        REQUIRE(container.size() == dims);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    REQUIRE(container[i, j, k] == Scalar{});
                }
            }
        }
    }

    SECTION("Copy Initialization") {
        ContainerSoa<Scalar, DIM> original(dims[0], dims[1], dims[2]);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    original[i, j, k] = Scalar(dims[1] * dims[2] * i + dims[2] * j + k);
                }
            }
        }
        ContainerSoa<Scalar, DIM> copy = original;
        REQUIRE(copy.size() == original.size());
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    REQUIRE(copy[i, j, k] == Scalar(dims[1] * dims[2] * i + dims[2] * j + k));
                }
            }
        }
    }

    SECTION("Move Initialization") {
        ContainerSoa<Scalar, DIM> original(dims[0], dims[1], dims[2]);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    original[i, j, k] = Scalar(dims[1] * dims[2] * i + dims[2] * j + k);
                }
            }
        }
        ContainerSoa<Scalar, DIM> moved = std::move(original);
        REQUIRE(moved.size() == dims);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    REQUIRE(moved[i, j, k] == Scalar(dims[1] * dims[2] * i + dims[2] * j + k));
                }
            }
        }
    }

    //TODO: Add container resizing tests.
    SECTION("Resize Container") {
        ContainerSoa<Scalar, DIM> container(dims[0], dims[1], dims[2]);
    }
}