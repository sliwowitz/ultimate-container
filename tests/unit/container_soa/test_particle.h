#pragma once

#include "container_soa/container_soa.h"
#include "vector/vector.h"

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;

template<typename TPosition,
        typename TMass,
        typename TIndex = int>
struct ParticleT
{
    ParticleT(TPosition::const_reference_vector in_r,
              const TMass& in_mass,
              const TIndex& in_id)
            : position(in_r),
              mass(in_mass),
              id(in_id)
    {}

    ParticleT(TPosition::reference_vector in_r,
              TMass& in_mass,
              TIndex& in_id)
            : position(in_r),
              mass(in_mass),
              id(in_id)
    {}

    ParticleT(const TPosition::value_vector& in_r,
              TMass in_mass,
              TIndex in_id)
            : position(in_r),
              mass(in_mass),
              id(in_id)
    {}

    ParticleT()
            : position(TPosition::Zeros()),
              mass(0),
              id(0)
    {}

    template<typename TPosition2, typename TMass2, typename TIndex2>
    ParticleT& operator=(const ParticleT<TPosition2, TMass2, TIndex2>& p){
        position = p.position;
        mass = p.mass;
        id = p.id;
        return *this;
    }

    TPosition position;
    TMass mass;
    TIndex id;

    using reference = ParticleT<typename TPosition::reference_vector, TMass&, TIndex&>;
    using const_reference = ParticleT<typename TPosition::const_reference_vector, const TMass&, const TIndex&>;
};

template<typename TPosition, typename TMass>
using Particle = ParticleT<TPosition, TMass>;

namespace ultimate_container::container_soa {
    template<std::size_t DIM, typename TPosition, typename TMass>
    struct ContainerSoaTraits<Particle<TPosition, TMass>, DIM> {
        using value_type = Particle<TPosition, TMass>;
        using reference  = Particle<TPosition, TMass>::reference;
        using const_reference = Particle<TPosition, TMass>::const_reference;
        using soa_tuple = std::tuple<
        ContainerSoa<TPosition, DIM>,
        ContainerSoa<TMass, DIM>,
        ContainerSoa<int, DIM>
        >;
    };

}