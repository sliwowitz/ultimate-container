#include <catch2/catch_all.hpp>
#include "vector/vector.h"

using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

TEST_CASE("Expressions: Basic Arithmetics", "[Expressions][Basic]") {
    Vector<int, 3> a = {1, 2, 3};
    Vector<int, 3> b = {4, 5, 6};

    SECTION("Addition") {
        auto c = a + b;
        REQUIRE(c[0] == 5);
        REQUIRE(c[1] == 7);
        REQUIRE(c[2] == 9);
    }

    SECTION("Subtraction") {
        auto c = a - b;
        REQUIRE(c[0] == -3);
        REQUIRE(c[1] == -3);
        REQUIRE(c[2] == -3);
    }

    SECTION("Scalar Multiplication") {
        auto c = a * 2;
        REQUIRE(c[0] == 2);
        REQUIRE(c[1] == 4);
        REQUIRE(c[2] == 6);
    }

    SECTION("Scalar Division") {
        auto c = a / 2;
        REQUIRE(c[0] == 0);
        REQUIRE(c[1] == 1);
        REQUIRE(c[2] == 1);
    }
}
