#include <catch2/catch_all.hpp>
#include "vector/vector.h"

using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

TEST_CASE("Expressions: Free-standing Operations", "[Expressions][Freestanding]") {
    Vector<int, 3> a = {1, 2, 3};
    Vector<int, 3> b = {4, 5, 6};

    SECTION("Dot Product") {
        auto result = dot(a, b);
        REQUIRE(result == 32);
    }

    SECTION("Cross Product") {
        auto result = cross(a, b);
        REQUIRE(result[0] == -3);
        REQUIRE(result[1] == 6);
        REQUIRE(result[2] == -3);
    }
}
