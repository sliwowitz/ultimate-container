#include <catch2/catch_all.hpp>
#include "vector/vector.h"

using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

TEST_CASE("Expressions: Long Expressions", "[Expressions][Long]") {
    Vector<int, 3> a = {1, 2, 3};
    Vector<int, 3> b = {4, 5, 6};
    int scalar = 2;

    SECTION("Long Chained Expression") {
        auto result = ((a + b) * scalar - b).cwise_min(a);
        REQUIRE(result[0] == 1);
        REQUIRE(result[1] == 2);
        REQUIRE(result[2] == 3);
    }
}
