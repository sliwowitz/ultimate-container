#include <catch2/catch_all.hpp>
#include "vector/vector.h"
#include "float_compare.h"

using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

TEST_CASE("Expressions: Type and Size Combinations", "[Expressions][TypeMixing]") {
    SECTION("Float and Double Vectors") {
        Vector<float, 3>  a = {1.1f, 2.2f, 3.3f};
        Vector<double, 3> b = {4.4,  5.5,  6.6};
        auto result = a + b;
        REQUIRE(is_approximately(static_cast<float>(result[0]), 5.5f));
        REQUIRE(is_approximately(static_cast<float>(result[1]), 7.7f));
        REQUIRE(is_approximately(static_cast<float>(result[2]), 9.9f));
    }

}