#pragma once

#include <cmath>
#include <complex>
#include <concepts>
#include <type_traits>

// Float comparators

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

#ifndef FLOAT_EQUALITY_DEFAULT_ERROR_FACTOR
#define FLOAT_EQUALITY_DEFAULT_ERROR_FACTOR 5.0
#endif

template<std::floating_point T>
constexpr inline bool is_exactly(const T a, const T b) {
    return a == b;
}

template<std::floating_point T>
constexpr inline bool
is_approximately(const T a, const T b, const T error_factor = FLOAT_EQUALITY_DEFAULT_ERROR_FACTOR) {
    return a == b                 // Operator || in C++ will short-circuit - if the floats are exactly equal, this operation will be fast
           || std::abs(a - b)
              < std::max(std::abs(a), std::abs(b))
                * std::numeric_limits<T>::epsilon()
                * error_factor;
}

#pragma GCC diagnostic pop