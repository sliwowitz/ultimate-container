#include <tuple>
#include <type_traits>
#include <catch2/catch_all.hpp>
#include "vector/vector.h"
#include "vector_test_matrix.h"

using namespace ultimate_container::vector;

template<typename TVector, typename ValueType, std::size_t... Is>
void test_element_access_and_modification_with_get(TVector& v, std::index_sequence<Is...>) {
    // Initialize elements
    ((std::get<Is>(v) = static_cast<ValueType>(Is + 1)), ...);

    // Check initialization
    auto check_initialization = [&](auto index) {
        REQUIRE(std::get<index>(v) == static_cast<ValueType>(index + 1));
    };
    (check_initialization(std::integral_constant<std::size_t, Is>{}), ...);

    // Modify elements
    ((std::get<Is>(v) = static_cast<ValueType>(sizeof...(Is) - Is)), ...);

    // Check modification
    auto check_modification = [&](auto index) {
        REQUIRE(std::get<index>(v) == static_cast<ValueType>(sizeof...(Is) - index));
    };
    (check_modification(std::integral_constant<std::size_t, Is>{}), ...);
}

template <typename T, typename U>
class is_assignable {
private:
    template <typename X, typename Y>
    static auto test(int) -> decltype(std::declval<X&>() = std::declval<Y>(), std::true_type());

    template <typename, typename>
    static auto test(...) -> std::false_type;

public:
    static constexpr bool value = decltype(test<T, U>(0))::value;
};

template <typename T, typename U>
constexpr void check_not_assignable() {
    static_assert(!is_assignable<T, U>::value, "Type is assignable when it should be read-only");
}

TEMPLATE_LIST_TEST_CASE("Vector Element Access and Modification", "[vector][access_modification]", AllVectorTypes) {
    using TestVector = TestType;
    constexpr std::size_t dim = TestVector::dimension;
    using ValueType = typename TestVector::value_type;

    SECTION("Element Access and Modification with Index Operator") {
        TestVector v;
        for (std::size_t i = 0; i < dim; ++i) v[i] = static_cast<ValueType>(i);
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v[i] == static_cast<ValueType>(i));

        for (std::size_t i = 0; i < dim; ++i) v[i] += static_cast<ValueType>(i);
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v[i] == static_cast<ValueType>(2*i));
    }

    SECTION("Element Access and Modification with std::get") {
        TestVector v;
        test_element_access_and_modification_with_get<TestVector, ValueType>(v, std::make_index_sequence<dim>{});
    }

    SECTION("Pointer and Reference Modifications") {
        TestVector v;
        for (std::size_t i = 0; i < dim; ++i) v[i] = static_cast<ValueType>(i + 1);

        // Modify via pointer
        for (std::size_t i = 0; i < dim; ++i) {
            ValueType* ptr = &v[i];
            *ptr = static_cast<ValueType>(dim - i);
        }
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v[i] == static_cast<ValueType>(dim - i));

        // Modify via reference
        for (std::size_t i = 0; i < dim; ++i) {
            ValueType& ref = v[i];
            ref = static_cast<ValueType>(i + 1);
        }
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v[i] == static_cast<ValueType>(i + 1));
    }

    SECTION("Const Correctness") {
        const TestVector v_const;
        for (std::size_t i = 0; i < dim; ++i) {
            // Ensure that we can access elements but not modify them
            auto value = v_const[i];
            static_cast<void>(value);  // Suppress unused variable warning
            INFO("Checking if v_const[" << i << "] is not assignable.");
            check_not_assignable<decltype(v_const[i]), int>();
        }
    }
}
